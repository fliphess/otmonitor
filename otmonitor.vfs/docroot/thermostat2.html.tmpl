%# -*- html -*-
% proc onoff {val} {
%   return [lindex {on off} [expr {!$val}]]
% }
% global gui
% array set val {
%     setpoint      20
%     temperature   20
%     outside       15
%     flame         0
%     chmode        0
%     dhwmode       0
%     override      0
% }
% foreach n [array names val] {
%     if {[info exists gui($n)]} {set val($n) $gui($n)}
% }
% scan [format %.1f $val(setpoint)] %d%s setpoint half
% if {[catch {format %.1f $val(outside)} outtemp]} {set outtemp $val(outside)}
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    [include includes/header.inc]
    <link rel="stylesheet" type="text/css" href="/static/css/thermostat.css">
    <script src="static/js/status.js" type="text/javascript"></script>
</head>
<body>
    [include includes/navbar.inc]

    <div class="container">
        <div class="main">
            <br>
            <div class="control-section">
                <form method="post">
                    <table>
                        <tr>
                            <tr>
                                <th colspan="8">
                                    <button class="btn-connection" id="connection" disabled>DISCONNECTED</button>
                                    <span class="control-title">Heating</span>
                                </th>
                            </tr>

                            <br/>

                            <tr>
                                <td colspan="8" align="center">
                                    Inside temperature:
                                    <span id="temperature">[format %.2f [get gui(roomtemp) 20]]</span>&deg;C
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"></td>
                                <td valign="center" width="40">
                                    <span id="container">
                                        <img id="chmode" src="images/chmode-[onoff [get gui(chmode) 0]].png" image0="images/chmode-off.png" image1="images/chmode-on.png">
                                        <img id="dhwmode" src="images/dhwmode-[onoff [get gui(dhwmode) 0]].png" image0="images/dhwmode-off.png" image1="images/dhwmode-on.png">
                                    </span>
                                </td>
                                <td width="20%"></td>
                                <td align="right" rowspan="2" width="100">
                                    <div class="setpoint" id="units">[expr {int([get gui(setpoint) 20])}]</div>
                                    <input type="text" id="setpoint" style="display: none;" onchange="parts(this, 'units', 'half')" value="[get gui(setpoint) 20]">
                                </td>
                                <td valign="center">
                                    <div class="half">&deg;C</div>
                                </td>
                                <td width="20%"></td>
                                <td width="40" align="center">
                                    <span class="btn-updown">
                                        <button type="button" id="up" class="btn-updown" onclick="return control('Up')">+</button>
                                    </span>
                                </td>
                                <td width="20%"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td valign="center">
                                    <img id="flame" src="images/flame-[onoff [get gui(flame) 0]].png" image0="images/flame-off.png" image1="images/flame-on.png">
                                </td>
                                <td></td>
                                <td valign="center">
                                    <div id="half" class="half">$half</div>
                                </td>
                                <td></td>
                                <td align="center">
                                    <span class="btn-updown">
                                        <button type="button" id="down" class="btn-updown" onclick="return control('Down')">-</button>
                                    </span>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="8" align="center">
                                    Outside temperature:
                                    <span id="outside">$outtemp</span>&deg;C
                                </td>
                            </tr>
                        </tr>
                    </table>
                </form>
            </div>

            <br>

            <div class="prog-section">
            <table>
                <tr>
                    <td>
                        <div id="scheduler" class="scheduler">Thermostat schedule</div>
                        <button id="prog" class="btn-program">ON</button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="status" class="status">Manually override temperature</div>
                        <input  type="hidden" class="override" id="override" value=[expr {int($val(override))}]></input >
                        <button id="manual" class="btn-manual" disabled>OFF</button>
                    </td>
                </tr>
            </table>
            </div>

            <br>

            <div class="modes-section">
            <table>
                <tr>
                    <td id="away">
                        <div class="btn-lg-label">Away</div>
                        <span id="away-value"></span><span>&deg;</span>
                    </td>
                    <td id="home">
                        <div class="btn-lg-label">Home</div>
                        <span id="home-value"></span><span>&deg;</span>
                    </td>
                </tr>
                <tr>
                    <td id="sleep">
                        <div class="btn-lg-label">Sleep</div>
                        <span id="sleep-value"></span><span>&deg;</span>
                    </td>
                    <td id="comfort">
                        <div class="btn-lg-label">Comfort</div>
                        <span id="comfort-value"></span><span>&deg;</span>
                    </td>
                </tr>
            </table>
            </div>

            <br>

        </div>
    </div>
    <script src="/static/js/thermostat.js" type="text/javascript"></script>
</body>
</html>
