<!-- *** NAVBAR ************************************************************ -->
<nav class="navbar navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="showMenu" class="navbar-brand" href="\">Opentherm Gateway</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="dashboard.html">Dashboard</a></li>
                <li><a href="status.html">Status</a></li>
                <li><a href="graph.html">Graph</a></li>
                <li><a href="messages.html">Messages</a></li>
                <li><a href="summary.html">Statistics</a></li>
                <li><a href="thermostat2.html">Thermostat</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="active"><a href="settings.html">Settings</a></li>
                        <li><a href="alert.html">Alert</a></li>
                        <li><a href="upgrade.html">Firmware Upgrade</a></li>
                        <li><a href="otgw_settings.html">Gateway Settings</a></li>
                        <li><a href="boiler_settings.html">Boiler Settings</a></li>
                        <li><a href="error_counters.html">Error Counters</a></li>
                        <li><a href="otm_config.html">OTMonitor Default Values</a></li>
                        <li><a href="otm_parameters.html">OTMonitor Commandline Parameters</a></li>
                        <li><a href="id_mapping.html">Opentherm ID Mapping</a></li>
                        <li><a rel="noopener noreferrer" target="_blank" href="/json">json API</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
