#!/bin/bash
TIMESTAMP="$( date '+%Y%m%d%H%M' )"

test -e /tmp/otmonitor && rm -rf /tmp/otmonitor

echo "Building otmonitor"
./kit/tclkit kit/sdx.kit wrap /tmp/otmonitor -runtime kit/runtime

echo -ne "Moving in place"
test -f /home/homeassistant/otmonitor/otmonitor && mv /home/homeassistant/otmonitor/otmonitor "/tmp/otmonitor.${TIMESTAMP}"
test -f /tmp/otmonitor && mv /tmp/otmonitor /home/homeassistant/otmonitor/otmonitor
echo -ne " [OK]\n"

echo -ne "Restarting service"
service otmonitor restart
echo -ne " [OK]\n"
